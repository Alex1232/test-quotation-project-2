if not exist venv (
  ECHO "INIT VENV"
  CALL python -m venv venv || GOTO :exit
)
CALL venv\Scripts\activate || GOTO :exit


CALL python -m pip install --upgrade pip==22.0.1 pip-tools==6.6.0 doit==0.36.0 || GOTO :exit
CALL python -m pip install --upgrade -r requirements.txt || GOTO :exit


pushd frontend
CALL npm install --emoji true --no-progress --non-interactive || GOTO :exit
popd

CALL cd .. || GOTO :exit

:exit
popd
if "%errorlevel%" NEQ "0" exit /b %errorlevel%