import json

import requests


def get_headers():

    headers = [
        {
            'text': 'Код валюты',
            'align': 'start',
            'sortable': False,
            'value': 'name'
        },
        {'text': 'Название валюты', 'value': 'calories'},
        {'text': 'Цена', 'value': 'fat'},
        {'text': 'Дата котировки', 'value': 'carbs'},
        {'text': 'Номинал ', 'value': 'protein'}
    ]

    return headers


def get_selection_quote():
    code_currency = [
        {
            'id': 1,
            'text': 'Евро',
            'code': 'EUR',
            'icon': 'fas fa-euro-sign'
        },
        {
            'id': 2,
            'text': 'Доллар США',
            'code': 'USD',
            'icon': 'fas fa-dollar-sign'
        },
        {
            'id': 3,
            'text': 'Китайский юань',
            'code': 'CNY',
            'icon': 'fas fa-yen-sign'
        },
        {
            'id': 4,
            'text': 'Фунт стерлингов',
            'code': 'GBP',
            'icon': 'fas fa-pound-sign'
        },
        {
            'id': 5,
            'text': 'Датских крон',
            'code': 'DKK',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 6,
            'text': 'Австралийский доллар',
            'code': 'AUD',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 7,
            'text': 'Азербайджанский манат',
            'code': 'AZN',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 8,
            'text': 'Армянских драмов',
            'code': 'AMD',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 9,
            'text': 'Белорусский рубль',
            'code': 'BYN',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 10,
            'text': 'Гонконгских долларов',
            'code': 'HKD',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 11,
            'text': 'Индийских рупий',
            'code': 'INR',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 12,
            'text': 'Казахстанских тенге',
            'code': 'KZT',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 13,
            'text': 'Канадский доллар',
            'code': 'CAD',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 14,
            'text': 'Киргизских сомов',
            'code': 'KGS',
            'icon': 'fas fa-file-invoice-dollar'
        },
        {
            'id': 15,
            'text': 'Китайских юаней',
            'code': 'CNY',
            'icon': 'fas fa-file-invoice-dollar'
        },
    ]

    return code_currency


def get_currency_in_json():
    url = "https://www.cbr-xml-daily.ru/daily_json.js"
    response = requests.request("GET", url)
    result = response.text

    return json.loads(result)
