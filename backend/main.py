import uvicorn
from fastapi import FastAPI

from fastapi.middleware.cors import CORSMiddleware

from backend.actions import get_headers, get_selection_quote, get_currency_in_json

app = FastAPI()

origins = ["*"]

app.add_middleware(
    CORSMiddleware,
    allow_origins=origins,
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)


@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/currency_quotes")
def read_root():
    code_currency = get_selection_quote()
    return {'desserts': code_currency}


@app.get("/table_quote/{params}")
def read_root(params: str):
    json = get_currency_in_json()
    valute = json['Valute']
    date = f"{json['Date'][:10]} / {json['Date'][11:-6]}"

    currency_data = [
        {
            'name': i,
            'calories': valute[i]['Name'],
            'fat': f'{valute[i]["Previous"]} / {valute[i]["Value"]}',
            'carbs': date,
            'protein': valute[i]['Nominal'],
        }
        for i in valute
    ]

    def filter_currency(x):
        valut = params.split(',')
        for i in valut:
            if i == x['name']:
                return x

    chosen_currency = list(map(filter_currency, currency_data))
    clean_chosen_currency = list(filter(None, chosen_currency))

    headers = get_headers()

    return {
        'desserts': clean_chosen_currency,
        'headers': headers
    }


if __name__ == '__main__':
    uvicorn.run(app, host="127.0.0.1", port=8888)
