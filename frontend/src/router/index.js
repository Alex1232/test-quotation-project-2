import Vue from 'vue'
import Router from 'vue-router'
import BaseListQuote from '../components/currency_quotes/BaseListQuote'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'BaseListQuote',
      component: BaseListQuote
    }
  ]
})
